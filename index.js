// BÀI TẬP 1
var xuatKQ = function () {
  var a = document.getElementById("txt-num-1").value * 1;
  var b = document.getElementById("txt-num-2").value * 1;
  var c = document.getElementById("txt-num-3").value * 1;
  if (a > b && b > c) {
    document.getElementById(
      "ket-qua-1"
    ).innerHTML = `Thứ tự tăng dần là: ${c}, ${b}, ${a}`;
  } else if (a > b && c > b && a > c) {
    document.getElementById(
      "ket-qua-1"
    ).innerHTML = `Thứ tự tăng dần là: ${b}, ${c}, ${a}`;
  } else if (a > b && c > a) {
    document.getElementById(
      "ket-qua-1"
    ).innerHTML = `Thứ tự tăng dần là: ${b}, ${a}, ${c}`;
  } else if (b > a && b > c && a > c) {
    document.getElementById(
      "ket-qua-1"
    ).innerHTML = `Thứ tự tăng dần là: ${c}, ${a}, ${b}`;
  } else if (b > c && c > a) {
    document.getElementById(
      "ket-qua-1"
    ).innerHTML = `Thứ tự tăng dần là: ${a}, ${c}, ${b}`;
  } else {
    document.getElementById(
      "ket-qua-1"
    ).innerHTML = `Thứ tự tăng dần là: ${a}, ${b}, ${c}`;
  }
};

// BÀI TẬP 2

var guiLoiChao = function () {
  var tenNguoiDung = document.getElementById("txt-nguoi-dung").value;
  var B = document.getElementById("txt-bo").value;
  var M = document.getElementById("txt-me").value;
  var A = document.getElementById("txt-anh").value;
  var E = document.getElementById("txt-em").value;
  if (tenNguoiDung == B) {
    document.getElementById("ket-qua-2").innerHTML = `Xin chào Bố!`;
  } else if (tenNguoiDung == M) {
    document.getElementById("ket-qua-2").innerHTML = `Xin chào Mẹ!`;
  } else if (tenNguoiDung == A) {
    document.getElementById("ket-qua-2").innerHTML = `Xin chào Anh Trai!`;
  } else if (tenNguoiDung == E) {
    document.getElementById("ket-qua-2").innerHTML = `Xin chào Em Gái!`;
  } else {
    document.getElementById(
      "ket-qua-2"
    ).innerHTML = `<p style="color:red">Vui lòng chọn người dùng!</p>`;
  }
};

// BÀI TẬP 3
var demChanLe = function () {
  var soNguyen1 = document.getElementById("txt-so-1").value * 1;
  var soNguyen2 = document.getElementById("txt-so-2").value * 1;
  var soNguyen3 = document.getElementById("txt-so-3").value * 1;
  var soChan = 0;
  var soLe = 0;
  if (soNguyen1 % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }
  if (soNguyen2 % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }
  if (soNguyen3 % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }
  document.getElementById(
    "ket-qua-3"
  ).innerHTML = `Có ${soChan} số chẵn, ${soLe} số lẻ`;
};

// BÀI TẬP 4
var duDoan = function () {
  var canhThu1 = document.getElementById("txt-canh-1").value * 1;
  var canhThu2 = document.getElementById("txt-canh-2").value * 1;
  var canhThu3 = document.getElementById("txt-canh-3").value * 1;
  if (canhThu1 == canhThu2 && canhThu2 == canhThu3) {
    document.getElementById("ket-qua-4").innerHTML = "Là tam giác đều";
  } else if (
    (canhThu1 == canhThu2 && canhThu3 != canhThu1) ||
    (canhThu2 == canhThu3 && canhThu2 != canhThu1)
  ) {
    document.getElementById("ket-qua-4").innerHTML = "Là tam giác cân";
  } else if (
    Math.pow(canhThu1, 2) == Math.pow(canhThu2, 2) + Math.pow(canhThu3, 2) ||
    Math.pow(canhThu2, 2) == Math.pow(canhThu1, 2) + Math.pow(canhThu3, 2) ||
    Math.pow(canhThu3, 2) == Math.pow(canhThu1, 2) + Math.pow(canhThu2, 2)
  ) {
    document.getElementById("ket-qua-4").innerHTML = "Là tam giác vuông";
  } else {
    document.getElementById("ket-qua-4").innerHTML = "Là một tam giác khác";
  }
};
